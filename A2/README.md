# A2

A simple script to simulate the usage of Device Driver

## aiadrv

aiadrv is a character device that has the operations of open, read, write, and release.

## test.aia.driver.c

A simple script to access aiadrv. This program will read user input and then write it to the aiadrv. If the user hit Enter, this program will read back from the aiadrv and then print it to the terminal.

# How to Execute:

## Compile the sourcecode

- Navigate to the directory of this folder
- Execute "sudo su" command to get the root privilege
- Compile the sourcecode using the "make" command. It will create 2 output files : aiadrv.mod.o and aiadrv.ko

## Insert Module

- Insert module to the kernel with this command

```bash
insmod aiadrv.ko
```

- Execute "dmesg" to check whether the module is already inserted to the Kernel (If so, it will show "device class created correctly")

## Run the Test

```bash
./test
```

Type in a short string then hit enter.
