# B2

Program that controls USB device using device driver.

## driver.c

Device driver that controls USB Device.

# How to Execute:

## Insert the flash disk

- Insert a flash disk into a USB port on your computer.
- Add your flash disk to the USB device setting in Virtual Box, so that the flash disk can be read by the virtual box.

## Check the flash disk id

- Run the command below to see the flash disk id

```bash
lsusb
```

- In the driver.c, change the value of vendor id and product id (line 21) with the id of the flash disk that you are using

## Remove the flash disk from the USB port

- For now, u can remove your flash disk from the USB port (and insert it again later) to simulate the use of the device driver

## Compile the sourcecode

- Navigate to the directory of this folder
- Execute "sudo su" command to get the root privilege
- Compile the sourcecode using the "make" command. It will create 2 output files : driver.mod.o and driver.ko

## Remove default USB storage

- Usually by default, the usb_storage is used by another program. In order to load our usb device driver module, we need to remove the default usb_storage using the command below:

```bash
modprobe -r usb_storage
```

- If there is an error, then we need to add the program name that uses the usb_storage.

```bash
modprobe -r <name of the program> usb_storage
```

## Insert Module

- Insert module to the kernel with this command

```bash
insmod driver.ko
```

- Execute "dmesg" to check whether the module is already inserted to the Kernel (If so, it will show "registered new interface driver aia-USB Driver")

## How to simulate the device driver

- Insert the flash disk and then execute "dmesg" to check whether the flash disk is connected to the kernel (If so, it will show the detail information of the flash disk includes the idVendor, idProduct, Serial number, etc)

- Now remove the flash disk, then execute "dmesg" , it will show "aia usb drive removed"
