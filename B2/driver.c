#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/usb.h>

//probe function
//called on device insertion 
static int pen_probe(struct usb_interface *interface, const struct usb_device_id *id)
{
	printk(KERN_INFO"{*} aia usb drive (%04X:%04X) plugged\n", id->idVendor, id->idProduct);
	return 0;//to indicates that we will mannage this device
}

static void pen_disconnect(struct usb_interface *interface){
	printk(KERN_INFO"{*} aia usb drive removed\n");
}


//usb_device_id
static struct usb_device_id pen_table[] ={
	//id_vendor,id_product
	{ USB_DEVICE(0x0781, 0x5567)}, // menggunakan lsusb untuk mengetahui usb idnya, masukkan id vendor dan id productnya.
	{}
};
MODULE_DEVICE_TABLE(usb, pen_table);


//usb_driver
static struct usb_driver write_driver =
{
		.name = "aia-USB Driver",
		.id_table = pen_table, //usb_device_id
		.probe = pen_probe,
		.disconnect = pen_disconnect,

};

static int __init pen_init(void){
	int ret =-1;
	printk(KERN_INFO"[*]aia-USB constructor of driver");
	printk(KERN_INFO"\tRegistering Driver with kernel");
	ret = usb_register(&write_driver);
	printk(KERN_INFO"\tRegistration is complete");
	return ret;
}

static void __exit pen_exit(void){
	printk(KERN_INFO"[*]aia-USB destructor of driver");
	usb_deregister(&write_driver);
	printk(KERN_INFO"\tunregisteration complete");
	
}
module_init(pen_init);
module_exit(pen_exit);
MODULE_LICENSE("GPL");            
MODULE_AUTHOR("Kelompok-aia");    
MODULE_DESCRIPTION("Simple script usb driver");
